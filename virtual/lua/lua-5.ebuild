# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib-minimal

DESCRIPTION="Virtual for Lua programming language"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~mips ~ppc ~s390 ~sh ~sparc ~x86 ~ppc-aix ~amd64-fbsd ~x86-fbsd ~amd64-linux ~arm-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE="luajit bit bit32"

# luajit is use-masked for these. I'm not sure if this applies to
# this luajit, but I have no hardware to test
REQUIRED_USE="luajit? ( !arm64 !ppc !ppc64 )"

RDEPEND="
	!luajit? (
		|| (
			dev-lang/lua:5.1[deprecated,${MULTILIB_USEDEP}]
			dev-lang/lua:5.2[deprecated,${MULTILIB_USEDEP}]
			dev-lang/lua:5.3[deprecated,${MULTILIB_USEDEP}]
		)
	)
	bit? (
		|| (
			dev-lang/luajit:2[${MULTILIB_USEDEP}]
			dev-lua/LuaBitOp[${MULTILIB_USEDEP}]
		)
	)
	bit32? (
		|| (
			dev-lang/lua:5.2[deprecated,${MULTILIB_USEDEP}]
			dev-lang/lua:5.3[deprecated,${MULTILIB_USEDEP}]
			dev-lua/bit32[${MULTILIB_USEDEP}]
		)
	)
	luajit? (
		dev-lang/luajit:2[${MULTILIB_USEDEP}]
		app-eselect/eselect-luajit
	)
	app-eselect/eselect-lua
	!!dev-lang/lua:0
"
DEPEND="${RDEPEND}"
S="${WORKDIR}"
