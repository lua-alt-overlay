# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit autotools git-r3 lua-utils-alt

DESCRIPTION="Simon's Improved Layout Engine "
HOMEPAGE="http://sile-typesetter.org"
EGIT_REPO_URI="https://github.com/sile-typesetter/sile"
EGIT_SUBMODULES=()

if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~sparc ~x86"

RDEPEND="
	dev-libs/libtexpdf
	dev-lua/cassowary
	dev-lua/cliargs
	dev-lua/cosmo
	dev-lua/epnf
	dev-lua/linenoise
	dev-lua/lpeg
	dev-lua/luaexpat
	dev-lua/luafilesystem
	dev-lua/luasec
	dev-lua/luasocket
	dev-lua/lua-zlib
	dev-lua/repl
	dev-lua/stdlib
	dev-lua/vstruct
	lua_targets_lua5_1? ( dev-lua/bit32[lua_targets_lua5_1] dev-lua/compat53[lua_targets_lua5_1] )
	lua_targets_lua5_2? ( dev-lua/compat53[lua_targets_lua5_2] )
	lua_targets_luajit2? ( dev-lua/bit32[lua_targets_luajit2] dev-lua/compat53[lua_targets_luajit2] )
"

src_prepare(){
	default
	eautoreconf
	rm -rf vendor
	#lua-alt_src_prepare
}

src_configure() {
	ECONF_SOURCE="${S}" econf \
		--with-system-expat \
		--with-system-filesystem \
		--with-system-libtexpdf \
		--with-system-lpeg \
		--with-system-luarocks \
		--with-system-socket \
		--with-system-zlib
}

src_install() {
	emake DESTDIR="${D}" install
}
