# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils multilib multilib-minimal portability pax-utils toolchain-funcs flag-o-matic check-reqs git-r3

DESCRIPTION="Just-In-Time Compiler for the Lua programming language"
HOMEPAGE="https://luajit.org/"
EGIT_REPO_URI="https://luajit.org/git/luajit-2.0.git"
EGIT_BRANCH="v2.1"
EGIT_MIN_CLONE_TYPE="single"
EGIT_COMMIT="v${PV/_/-}"

SLOT="2"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~mips ~ppc ~s390 ~sh ~sparc ~x86 ~ppc-aix ~amd64-fbsd ~x86-fbsd ~amd64-linux ~arm-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

LICENSE="MIT"
IUSE="debug valgrind lua52compat +optimization"

RDEPEND="
	valgrind? ( dev-util/valgrind )
"
DEPEND="
	${RDEPEND}
	app-eselect/eselect-luajit
	app-eselect/eselect-lua
"

# Arm64 and ppc have luajit keyworded upstream, so we can't really
# change them. However, virtual/lua has to run everywhere. I can't
# quite figure out how to make repoman understand that the luajit
# USE flag will never be applied in these cases, so handle it here
# as well.
PDEPEND="
	!ppc64? ( !ppc? ( !arm64? ( virtual/lua[luajit] ) ) )
"

HTML_DOCS=( "doc/." )

MULTILIB_WRAPPED_HEADERS=(
	"/usr/include/luajit-${SLOT}/luaconf.h"
)

check_req() {
	if use optimization; then
		CHECKREQS_MEMORY="300M"
		check-reqs_pkg_${1}
	fi
}

pkg_pretend() {
	check_req pretend
}

pkg_setup() {
	check_req setup
}

src_prepare() {
	# fixing prefix and version
	sed -r \
		-e "s|^(VERSION)=.*|\1=${PV}|" \
		-e 's|^(INSTALL_SONAME)=.*|\1=$(INSTALL_SOSHORT1).$(VERSION)|' \
		-e "s|^(INSTALL_PCNAME)=.*|\1=${P}.pc|" \
		-e 's|^(FILE_MAN)=.*|\1=${P}.1|' \
		-e "s|( PREFIX)=.*|\1=${EROOT}/usr|" \
		-i Makefile || die "failed to fix prefix in Makefile"

	use debug && (
		sed -r \
			-e 's/#(CCDEBUG= -g)/\1 -O0/' \
			-i src/Makefile || die "Failed to enable debug"
	)
	mv "${S}"/etc/${PN}.1 "${S}"/etc/${P}.1

	eapply "${FILESDIR}/luajit-2.0-no-cwd-in-default-path.patch"

	eapply_user

	multilib_copy_sources
}

multilib_src_configure() {
	sed -r \
		-e "s|( MULTILIB)=.*|\1=$(get_libdir)|" \
		-i Makefile || die "failed to fix libdir in Makefile"
	sed -r \
		-e "s|(prefix)=.*|\1=${EROOT}/usr|" \
		-e "s|(multilib)=.*|\1=$(get_libdir)|" \
		-i "etc/${PN}.pc" || die "Failed to slottify"
}

multilib_src_compile() {
	local opt xcflags;
	use optimization && opt="amalg";

	tc-export CC

	xcflags=(
		$(usex lua52compat "-DLUAJIT_ENABLE_LUA52COMPAT" "")
		$(usex debug "-DLUAJIT_USE_GDBJIT" "")
		$(usex valgrind "-DLUAJIT_USE_VALGRIND" "")
		$(usex valgrind "-DLUAJIT_USE_SYSMALLOC" "")
		$(usex amd64 "-DLUAJIT_ENABLE_GC64" "")
#		$(usex arm64 "-DLUAJIT_ENABLE_GC64" "")
#		$(usex mips64 "-DLUAJIT_ENABLE_GC64" "")
#		$(usex ppc64 "-DLUAJIT_ENABLE_GC64" "")
#		$(usex s390_64 "-DLUAJIT_ENABLE_GC64" "")
	)

	emake \
		Q= \
		HOST_CC="$(tc-getCC)" \
		CC="${CC}" \
		TARGET_STRIP="true" \
		XCFLAGS="${xcflags[*]}" ${opt}
}

multilib_src_install() {
	emake DESTDIR="${D}" MULTILIB="$(get_libdir)" install

	einstalldocs

	host-is-pax && pax-mark m "${ED}/usr/bin/${P}"
	newman "etc/${P}.1" "luacjit-${PV}.1"
	newbin "${FILESDIR}/luac.jit" "luacjit-${PV}"
	ln -fs "${P}" "${ED}/usr/bin/${PN}-${SLOT}"
}

pkg_postinst() {
	if [[ ! -n $(readlink "${EROOT}/usr/bin/luajit") ]] ; then
		eselect luajit set luajit-${PV}
	fi
	if [[ ! -n $(readlink "${EROOT}/usr/bin/lua") ]] ; then
		eselect lua set jit-${PV}
	fi
}
