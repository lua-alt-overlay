# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="This is a lua port of the cassowary constraint solving toolkit"
HOMEPAGE="https://github.com/sile-typesetter/cassowary.lua"
EGIT_REPO_URI="https://github.com/sile-typesetter/cassowary.lua"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="Apache-2.0"
SLOT="0"
DEPEND="${LUA_DEPS}
	dev-lua/penlight
	virtual/pkgconfig
"

src_install() {
	this_install() {
		# When this upgrades to 2.3, you'll want
		# insinto "${INSTALL_LMOD}/cassowary"
		# doins "cassowary/init.lua"

		insinto "${INSTALL_LMOD}"
		doins "cassowary.lua"
	}
	lua_foreach_impl this_install
}
