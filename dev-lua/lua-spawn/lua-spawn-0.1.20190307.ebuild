# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="A lua library to spawn programs"
HOMEPAGE="https://github.com/daurnimator/lua-spawn"
EGIT_REPO_URI="https://github.com/daurnimator/lua-spawn"
EGIT_COMMIT="b2f1629754a3e78edab1f69c71e7d7334cbe4e92"
KEYWORDS="~amd64 ~arm ~x86"

LICENSE="MIT"
SLOT="0"

PATCHES=( "${FILESDIR}/${P}_makefile.patch" )

DEPEND="
	${LUA_DEPS}
	dev-lua/lunix
"
RDEPEND="
	${DEPEND}
"
