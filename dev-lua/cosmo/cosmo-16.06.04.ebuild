# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="Cosmo is a 'safe templates' engine."
HOMEPAGE="https://github.com/mascarenhas/cosmo"
EGIT_REPO_URI="https://github.com/mascarenhas/cosmo"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	>=dev-lua/lpeg-0.9
	virtual/pkgconfig
"

src_install() {
	this_install() {
		cp "src/cosmo.lua" "src/init.lua"

		insinto "${INSTALL_LMOD}/cosmo"
		doins "src/init.lua"
		doins "src/cosmo/fill.lua"
		doins "src/cosmo/grammar.lua"
	}
	lua_foreach_impl this_install
}
