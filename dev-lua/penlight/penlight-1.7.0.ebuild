# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="Penlight brings together a set of generally useful pure Lua modules"
HOMEPAGE="https://github.com/Tieske/Penlight"
EGIT_REPO_URI="https://github.com/Tieske/Penlight"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	dev-lua/luafilesystem
	virtual/pkgconfig
"

src_install() {
	this_install() {
		insinto "${INSTALL_LMOD}/pl"
		doins "lua/pl/init.lua"
		doins "lua/pl/strict.lua"
		doins "lua/pl/dir.lua"
		doins "lua/pl/operator.lua"
		doins "lua/pl/input.lua"
		doins "lua/pl/config.lua"
		doins "lua/pl/seq.lua"
		doins "lua/pl/stringio.lua"
		doins "lua/pl/text.lua"
		doins "lua/pl/test.lua"
		doins "lua/pl/tablex.lua"
		doins "lua/pl/app.lua"
		doins "lua/pl/stringx.lua"
		doins "lua/pl/lexer.lua"
		doins "lua/pl/utils.lua"
		doins "lua/pl/compat.lua"
		doins "lua/pl/sip.lua"
		doins "lua/pl/permute.lua"
		doins "lua/pl/pretty.lua"
		doins "lua/pl/class.lua"
		doins "lua/pl/List.lua"
		doins "lua/pl/data.lua"
		doins "lua/pl/Date.lua"
		doins "lua/pl/luabalanced.lua"
		doins "lua/pl/comprehension.lua"
		doins "lua/pl/path.lua"
		doins "lua/pl/array2d.lua"
		doins "lua/pl/func.lua"
		doins "lua/pl/lapp.lua"
		doins "lua/pl/file.lua"
		doins "lua/pl/template.lua"
		doins "lua/pl/Map.lua"
		doins "lua/pl/MultiMap.lua"
		doins "lua/pl/OrderedMap.lua"
		doins "lua/pl/Set.lua"
		doins "lua/pl/xml.lua"
		doins "lua/pl/url.lua"
		doins "lua/pl/types.lua"
		doins "lua/pl/import_into.lua"
	}
	lua_foreach_impl this_install
}
