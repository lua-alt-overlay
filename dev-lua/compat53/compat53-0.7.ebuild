# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 luajit2 )

inherit toolchain-funcs git-r3 lua-alt

DESCRIPTION="Lua-5.3-style APIs for Lua 5.2 and 5.1"
HOMEPAGE="https://keplerproject.github.io/lua-compat-5.3/"
EGIT_REPO_URI="https://github.com/keplerproject/lua-compat-5.3"
EGIT_COMMIT="v${PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~mips ~x86 ~x86-fbsd"

DEPEND="${LUA_DEPS}
	virtual/pkgconfig"

HTML_DOCS=( README.md )

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
}
