# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="Linenoise is a delightfully simple command line library. This Lua module is simply a binding for it."
HOMEPAGE="https://github.com/hoelzro/lua-linenoise"
EGIT_REPO_URI="https://github.com/hoelzro/lua-linenoise"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	virtual/pkgconfig
"

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
}

src_install() {
	this_install() {
		insinto "${INSTALL_CMOD}"
		doins "linenoise.so"
	}
	lua_foreach_impl this_install
}
