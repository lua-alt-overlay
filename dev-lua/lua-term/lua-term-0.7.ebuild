# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="a Lua module for manipulating a terminal"
HOMEPAGE="https://github.com/hoelzro/lua-term"
EGIT_REPO_URI="https://github.com/hoelzro/lua-term"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="0.07"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="
	${DEPEND}
"

DOCS=(README.md)

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
}
