# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="get and set terminal attributes, line control, get and set baud rate"
HOMEPAGE="https://github.com/sam-github/termios-lua"
EGIT_REPO_URI="https://github.com/sam-github/termios-lua"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="8b5c837075ce6e90e0aa1a7fe25dbdd656f669fd"
	KEYWORDS="~amd64"
fi

LICENSE="BSD-2"
SLOT="0"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="
	${DEPEND}
"

DOCS=(README.txt)

PATCHES=(
	"${FILESDIR}/${P}-luaL_reg.patch"
	"${FILESDIR}/${P}-luaL_checkint.patch"
	"${FILESDIR}/${P}-luaL_optint.patch"
	"${FILESDIR}/${P}-luaL_register.patch"
)

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
}
