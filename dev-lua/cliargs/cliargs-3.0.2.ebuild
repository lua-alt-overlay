# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="cliargs is a command-line argument parser for Lua"
HOMEPAGE="https://github.com/amireh/lua_cliargs"
EGIT_REPO_URI="https://github.com/amireh/lua_cliargs"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV%.*}-${PV##*.}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	virtual/pkgconfig
"

src_install() {
	this_install() {
		insinto "${INSTALL_LMOD}"
		doins "src/cliargs.lua"

		insinto "${INSTALL_LMOD}/cliargs"
		doins "src/cliargs/config_loader.lua"
		doins "src/cliargs/constants.lua"
		doins "src/cliargs/core.lua"
		doins "src/cliargs/parser.lua"
		doins "src/cliargs/printer.lua"

		insinto "${INSTALL_LMOD}/cliargs/utils"
		doins "src/cliargs/utils/disect.lua"
		doins "src/cliargs/utils/disect_argument.lua"
		doins "src/cliargs/utils/filter.lua"
		doins "src/cliargs/utils/lookup.lua"
		doins "src/cliargs/utils/shallow_copy.lua"
		doins "src/cliargs/utils/split.lua"
		doins "src/cliargs/utils/trim.lua"
		doins "src/cliargs/utils/wordwrap.lua"
	}
	lua_foreach_impl this_install
}
