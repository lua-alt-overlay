# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="A reusable REPL component for Lua, written in Lua"
HOMEPAGE="https://github.com/hoelzro/lua-repl"
EGIT_REPO_URI="https://github.com/hoelzro/lua-repl"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	virtual/pkgconfig
"

src_compile() {
	:
}

src_install() {
	this_install() {
		insinto "${INSTALL_LMOD}/repl"
		doins "repl/init.lua"
		doins "repl/utils.lua"
		doins "repl/sync.lua"
		doins "repl/console.lua"
		doins "repl/compat.lua"

		insinto "${INSTALL_LMOD}/repl/plugins"
		doins "repl/plugins/autoreturn.lua"
		doins "repl/plugins/completion.lua"
		doins "repl/plugins/example.lua"
		doins "repl/plugins/history.lua"
		doins "repl/plugins/keep_last_eval.lua"
		doins "repl/plugins/linenoise.lua"
		doins "repl/plugins/pretty_print.lua"
		doins "repl/plugins/rcfile.lua"
		doins "repl/plugins/semicolon_suppress_output.lua"
		doins "repl/plugins/filename_completion.lua"
		doins "repl/plugins/rlwrap.lua"
	}
	lua_foreach_impl this_install

	dobin "rep.lua"
}
