# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit toolchain-funcs git-r3 lua-alt

MY_PV=${PV//./_}

DESCRIPTION="File System Library for the Lua Programming Language"
HOMEPAGE="https://keplerproject.github.io/luafilesystem/"
EGIT_REPO_URI="https://github.com/keplerproject/luafilesystem"
EGIT_COMMIT="v${MY_PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~mips ~x86 ~x86-fbsd"
IUSE="realpath"

DEPEND="${LUA_DEPS}
	virtual/pkgconfig"

HTML_DOCS=( doc/us )

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	use realpath && eapply "${FILESDIR}/20161018_realpath.patch"
	lua-alt_src_prepare
}
