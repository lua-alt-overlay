# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="Lua bindings for libtermkey"
HOMEPAGE="https://repo.or.cz/lua-termkey.git"
EGIT_REPO_URI="https://repo.or.cz/lua-termkey.git"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND="${LUA_DEPS}
	>=dev-libs/libtermkey-0.22"
RDEPEND="${DEPEND}"

DOCS=(README)
