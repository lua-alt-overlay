# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="Lua library to manipulate binary data"
HOMEPAGE="https://github.com/ToxicFrog/vstruct.git"
EGIT_REPO_URI="https://github.com/ToxicFrog/vstruct.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	virtual/pkgconfig
"

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
	eapply_user
}
