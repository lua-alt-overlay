# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="Pure Lua implementation of the wcwidth() function"
HOMEPAGE="http://www.inf.puc-rio.://github.com/aperezdc/lua-wcwidth"
EGIT_REPO_URI="https://github.com/aperezdc/lua-wcwidth"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	virtual/pkgconfig
"

src_install() {
	this_install() {
		insinto "${INSTALL_LMOD}"
		doins wcwidth.lua

		insinto "${INSTALL_LMOD}/wcwidth"
		doins wcwidth/init.lua
		doins wcwidth/widetab.lua
		doins wcwidth/zerotab.lua
	}
	lua_foreach_impl this_install
}
