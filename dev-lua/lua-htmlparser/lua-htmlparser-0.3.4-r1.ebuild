# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="Parse HTML text into a tree of elements with selectors"
HOMEPAGE="https://github.com/msva/lua-htmlparser"
EGIT_REPO_URI="https://github.com/msva/lua-htmlparser"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi

LICENSE="LGPL-3+"
SLOT="0"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="
	${DEPEND}
"

DOCS=(README.md, doc/sample.lua)

src_install() {
	this_install() {
		mkdir -p "${ED}/${INSTALL_LMOD}/htmlparser"
		cp "${BUILD_DIR}/src/htmlparser.lua" "${ED}/${INSTALL_LMOD}"
		cp "${BUILD_DIR}/src/htmlparser/ElementNode.lua" "${ED}/${INSTALL_LMOD}/htmlparser"
		cp "${BUILD_DIR}/src/htmlparser/voidelements.lua" "${ED}/${INSTALL_LMOD}/htmlparser"
	}
	lua_foreach_impl this_install
}
