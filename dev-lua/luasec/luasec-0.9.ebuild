# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit lua-alt git-r3

DESCRIPTION="LuaSec is a binding for OpenSSL library to provide TLS/SSL communication."
HOMEPAGE="https://github.com/brunoos/luasec/wiki"
EGIT_REPO_URI="https://github.com/brunoos/luasec"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="libressl"

RDEPEND="
	${LUA_DEPS}
	dev-lua/luasocket
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
"
DEPEND="
	${RDEPEND}
"

PATCHES=(
	"${FILESDIR}/${P}-bad-cflags.patch"
)

src_compile() {
	this_compile() {
		emake LD='$(CC)' LUAPATH="${INSTALL_LMOD}" LUACPATH="${INSTALL_CMOD}" linux
	}
	lua_foreach_impl this_compile
}

src_install() {
	this_install() {
		emake DESTDIR="${D}" LUAPATH="${INSTALL_LMOD}" LUACPATH="${INSTALL_CMOD}" install
	}
	lua_foreach_impl this_install
}
