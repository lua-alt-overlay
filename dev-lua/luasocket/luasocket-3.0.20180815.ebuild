# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit lua-alt git-r3

DESCRIPTION="Networking support library for the Lua language"
HOMEPAGE="http://www.tecgraf.puc-rio.br/~diego/professional/luasocket/"
EGIT_REPO_URI="https://github.com/diegonehab/luasocket"
EGIT_COMMIT="5813cd05054599b0409480d3214f1827c2360467"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

RDEPEND="${LUA_DEPS}"
DEPEND="${RDEPEND}"

S=${WORKDIR}/${PN}-${PV/_/-}
PATCHES=(
	"${FILESDIR}/luaL_checkinteger.patch"
)

src_install() {
	this_install() {
		emake \
			LUAPREFIX_linux=/usr \
			LUAV="${LUA_V}" \
			CDIR_linux="${INSTALL_CMOD}" \
			LDIR_linux="${INSTALL_LMOD}" \
			install-unix
	}
	lua_foreach_impl this_install

	dodoc README

	insinto "${PREFIX}/usr/include/luasocket"
	doins src/auxiliar.h
	doins src/buffer.h
	doins src/compat.h
	doins src/except.h
	doins src/inet.h
	doins src/io.h
	doins src/luasocket.h
	doins src/mime.h
	doins src/options.h
	doins src/pierror.h
	doins src/select.h
	doins src/socket.h
	doins src/tcp.h
	doins src/timeout.h
	doins src/udp.h
	doins src/unixdgram.h
	doins src/unix.h
	doins src/unixstream.h
	doins src/usocket.h
	doins src/wsocket.h
}
