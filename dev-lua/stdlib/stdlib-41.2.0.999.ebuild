# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="lua-stdlib is a library of modules for common programming tasks, including list and table operations, and pretty-printing"
HOMEPAGE="https://github.com/lua-stdlib/lua-stdlib"
EGIT_REPO_URI="https://github.com/lua-stdlib/lua-stdlib"
if [[ ${PV} == 9999 ]]
then
	:
elif [[ ${PV} == "41.2.0.999" ]]
then
	# Their git history is all screwed up. The commit that we actually want
	# (the one that adds support for 5.4) is just about the only commit in
	# the last five years without a version tag.
	EGIT_BRANCH="split-lua-tools"
	EGIT_COMMIT="58414c5fe459bbb317b95ea118cbbe61704ac588"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	>=dev-lua/normalize-2.0
	dev-lua/underscoredebug
	virtual/pkgconfig
"

src_compile() {
	:
}

src_install() {
	this_install() {
		insinto "${INSTALL_LMOD}"
		doins lib/*.lua
		
		insinto "${INSTALL_LMOD}/std"
		doins lib/std/*.lua

		insinto "${INSTALL_LMOD}/std/debug_init"
		doins lib/std/debug_init/*.lua
	}
	lua_foreach_impl this_install
}
