# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="An XML Parser written entirely in Lua that works for Lua 5.1 to 5.3"
HOMEPAGE="http://manoelcampos.github.io/xml2lua/"
EGIT_REPO_URI="https://github.com/manoelcampos/xml2lua"

MY_PV="$(ver_rs 2 -)"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="v${MY_PV}"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="
	${DEPEND}
"

DOCS=(README.md)

src_install() {
	this_install() {
		mkdir -p "${ED}/${INSTALL_LMOD}/xmlhandler"
		cp "${BUILD_DIR}/xml2lua.lua" "${ED}/${INSTALL_LMOD}"
		cp "${BUILD_DIR}/XmlParser.lua" "${ED}/${INSTALL_LMOD}"
		cp "${BUILD_DIR}/xmlhandler/tree.lua" "${ED}/${INSTALL_LMOD}/xmlhandler"
		cp "${BUILD_DIR}/xmlhandler/print.lua" "${ED}/${INSTALL_LMOD}/xmlhandler"
		cp "${BUILD_DIR}/xmlhandler/dom.lua" "${ED}/${INSTALL_LMOD}/xmlhandler"
	}
	lua_foreach_impl this_install
}
