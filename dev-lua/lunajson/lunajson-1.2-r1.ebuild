# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="A strict and fast JSON parser/decoder/encoder written in pure Lua."
HOMEPAGE="https://github.com/grafi-tt/lunajson"
EGIT_REPO_URI="https://github.com/grafi-tt/lunajson"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="${PV}"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="
	${DEPEND}
"

DOCS=(README.md)

src_install() {
	this_install() {
		mkdir -p "${ED}/${INSTALL_LMOD}/lunajson"
		cp "${BUILD_DIR}/src/lunajson.lua" "${ED}/${INSTALL_LMOD}"
		cp "${BUILD_DIR}/src/lunajson/decoder.lua" "${ED}/${INSTALL_LMOD}/lunajson"
		cp "${BUILD_DIR}/src/lunajson/encoder.lua" "${ED}/${INSTALL_LMOD}/lunajson"
		cp "${BUILD_DIR}/src/lunajson/sax.lua" "${ED}/${INSTALL_LMOD}/lunajson"
	}
	lua_foreach_impl this_install
}
