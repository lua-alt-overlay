# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit lua-alt git-r3

DESCRIPTION="Lua bindings to zlib"
HOMEPAGE="https://github.com/brimworks/lua-zlib"
EGIT_REPO_URI="https://github.com/brimworks/lua-zlib"

if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~sparc ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE=""

RDEPEND="${LUA_DEPS}
	sys-libs/zlib"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
}
