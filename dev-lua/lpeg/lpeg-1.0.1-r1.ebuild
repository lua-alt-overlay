# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit flag-o-matic toolchain-funcs lua-alt

DESCRIPTION="Parsing Expression Grammars for Lua"
HOMEPAGE="http://www.inf.puc-rio.br/~roberto/lpeg/"
SRC_URI="http://www.inf.puc-rio.br/~roberto/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~mips ~ppc ~s390 ~sh ~sparc ~x86 ~ppc-aix ~amd64-fbsd ~x86-fbsd ~amd64-linux ~arm-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

IUSE="debug doc"

DEPEND="
	${LUA_DEPS}
	virtual/pkgconfig
"

DOCS=( "HISTORY" )
HTML_DOCS=( "lpeg.html"  "re.html"  )
PATCHES=( "${FILESDIR}"/${P}-makefile.patch )

src_prepare() {
	use debug && append-cflags -DLPEG_DEBUG
	lua-alt_src_prepare
}

src_install() {
	this_install() {
		exeinto "${INSTALL_CMOD#${EPREFIX}}"
		doexe lpeg.so

		insinto "${INSTALL_LMOD#${EPREFIX}}"
		doins re.lua
	}
	lua_foreach_impl this_install

	use doc && einstalldocs
}
