# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit lua-alt git-r3

DESCRIPTION="This library transforms any Lua value into a human-readable representation."
HOMEPAGE="https://github.com/kikito/inspect.lua"
EGIT_REPO_URI="https://github.com/kikito/inspect.lua"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="MIT"
SLOT="0"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="
	${DEPEND}
"

DOCS=(README.md)

src_install() {
	this_install() {
		mkdir -p "${ED}/${INSTALL_LMOD}"
		cp "${BUILD_DIR}/inspect.lua" "${ED}/${INSTALL_LMOD}"
	}
	lua_foreach_impl this_install
}
