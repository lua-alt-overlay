# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# Not needed for Lua5.2. Needed again in lua5.3+ ?
LUA_COMPAT=( lua5_1 lua5_3 lualuajit2 )
EGIT_REPO_URI="https://github.com/keplerproject/lua-compat-5.2"
EGIT_COMMIT="v${PV}"

inherit git-r3 lua-alt

DESCRIPTION="A Lua5.2+ bit manipulation library"
HOMEPAGE="https://github.com/keplerproject/lua-compat-5.2"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~mips ~ppc ~s390 ~sh ~sparc ~x86 ~ppc-aix ~amd64-fbsd ~x86-fbsd ~amd64-linux ~arm-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

LICENSE="MIT"
SLOT="0"
IUSE=""

DOCS=(README.md)

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}"
	lua-alt_src_prepare
	eapply_user
}
