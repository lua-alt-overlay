# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit lua-alt git-r3

DESCRIPTION="Stackable Continuation Queues"
HOMEPAGE="http://25thandclement.com/~william/projects/cqueues.html"
EGIT_REPO_URI="https://25thandClement.com/~william/projects/cqueues.git"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="rel-${PV}"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="doc examples"

DEPEND="
	${LUA_DEPS}
"
RDEPEND="${DEPEND}"

DOCS=(doc/cqueues.pdf)
PATCHES=(
	"${FILESDIR}/${P}-makefile.patch"
	"${FILESDIR}/${P}-version-detection.patch"
	"${FILESDIR}/${P}-no-pragma.patch"
	"${FILESDIR}/${P}-5.3-deprecations.patch"
	"${FILESDIR}/${P}-5.4-lua_resume.patch"
)

src_prepare() {
	eapply_user

	cp config.h.guess config.h

	pushd src
	m4 errno.c.m4 > errno.c
	popd

	cp config.h src/config.h
	cp config.h src/lib/config.h

	lua-alt_src_prepare
}
