# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit lua-alt git-r3

DESCRIPTION="Lua Unix Module"
HOMEPAGE="http://25thandclement.com/~william/projects/lunix.html"
EGIT_REPO_URI="https://25thandClement.com/~william/projects/lunix.git"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="rel-${PV}"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="doc examples libressl"

DEPEND="
	${LUA_DEPS}
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
"
RDEPEND="${DEPEND}"

DOCS=(doc/.)
PATCHES=( "${FILESDIR}/${PN}-2018.11.22-makefile.patch" )
