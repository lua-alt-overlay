# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit eutils lua-alt

DESCRIPTION="LuaExpat is a SAX XML parser based on the Expat library"
HOMEPAGE="http://www.keplerproject.org/luaexpat/"
SRC_URI="http://matthewwild.co.uk/projects/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~sparc ~x86"

DEPEND="${LUA_DEPS}
	virtual/pkgconfig"

src_prepare() {
	lua-alt_src_prepare
	this_maybe_patch() {
		case "${MULTIBUILD_VARIANT}" in
		luajit2)
			eapply "${FILESDIR}/${P}-luajit-has-luaL_setfuncs.patch"
			;;
		*)
			;;
		esac
		eapply "${FILESDIR}/${P}-cflags.patch"
		eapply "${FILESDIR}/${P}-respect-dirs.patch"
		mv makefile Makefile
	}

	lua_foreach_impl this_maybe_patch
	rm makefile
}

src_compile() {
	this_compile() {
		emake DESTDIR="${D}" LUA_V="${LUA_V}" LUA_INCLUDEDIR="${LUA_INCLUDEDIR}" INSTALL_LMOD="${INSTALL_LMOD}" INSTALL_CMOD="${INSTALL_CMOD}"
	}

	lua_foreach_impl this_compile
}
src_install() {
	this_install() {
		emake DESTDIR="${D}" LUA_V="${LUA_V}" LUA_INCLUDEDIR="${LUA_INCLUDEDIR}" INSTALL_LMOD="${INSTALL_LMOD}" INSTALL_CMOD="${INSTALL_CMOD}" install
	}

	lua_foreach_impl this_install
}
