# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )

inherit eutils lua-alt

DESCRIPTION="LuaExpat is a SAX XML parser based on the Expat library"
HOMEPAGE="http://www.keplerproject.org/luaexpat/"
SRC_URI="http://matthewwild.co.uk/projects/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~sparc ~x86"

DEPEND="${LUA_DEPS}
	virtual/pkgconfig"

src_prepare() {
	lua-alt_src_prepare
	this_maybe_patch() {
		case "${MULTIBUILD_VARIANT}" in
		luajit2)
			eapply "${FILESDIR}/luajit-has-luaL_setfuncs.patch"
			;;
		*)
			;;
		esac
		eapply "${FILESDIR}/cflags.patch"
		eapply "${FILESDIR}/respect-dirs.patch"
	}

	lua_foreach_impl this_maybe_patch
}
