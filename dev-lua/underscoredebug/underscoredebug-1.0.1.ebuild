# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LUA_COMPAT=( lua5_1 lua5_2 lua5_3 lua5_4 luajit2 )
inherit eutils git-r3 lua-alt

DESCRIPTION="Debug hints library"
HOMEPAGE="https://github.com/lua-stdlib/_debug.git"
EGIT_REPO_URI="https://github.com/lua-stdlib/_debug.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="${LUA_DEPS}
	virtual/pkgconfig
"

src_install() {
	this_install() {
		insinto "${INSTALL_LMOD}/std/_debug"
		doins "lib/std/_debug/init.lua"
		doins "lib/std/_debug/version.lua"
	}
	lua_foreach_impl this_install
}
