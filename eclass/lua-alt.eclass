# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: lua-alt.eclass
# @MAINTAINER:
# ???????
# @AUTHOR:
# S. Gilles <sgilles@umd.edu>
# Based on work of: Michał Górny <mgorny@gentoo.org>
# Based on work of: Krzysztof Pawlik <nelchael@gentoo.org>
# @SUPPORTED_EAPIS: 6 7
# @BLURB: A common, simple eclass for Lua packages.
# @DESCRIPTION:
# A common eclass providing helper functions to build and install
# packages supporting being installed for multiple Lua
# implementations.
#
# This eclass sets correct IUSE. Modification of REQUIRED_USE has
# to be done by the author of the ebuild. lua-alt also provides
# methods to easily run a command for each enabled Lua implementation
# and duplicate the sources for them.

case "${EAPI:-0}" in
	0|1|2|3|4|5)
		die "Unsupported EAPI=${EAPI:-0} (too old) for ${ECLASS}"
		;;
	6|7)
		;;
	*)
		die "Unsupported EAPI=${EAPI} (unknown) for ${ECLASS}"
		;;
esac


inherit multibuild
inherit toolchain-funcs

inherit lua-utils-alt

EXPORT_FUNCTIONS src_prepare src_configure src_compile src_test src_install

# @ECLASS-VARIABLE: LUA_INCLUDEDIR
# @DESCRIPTION:
# Usable in phase functions. Set to the "includedir" variable, as
# given by pkg-config, for the relevant lua target

# @ECLASS-VARIABLE: LUA_LIBDIR
# @DESCRIPTION:
# Usable in phase functions. Set to the "libdir" variable, as
# given by pkg-config, for the relevant lua target

# @ECLASS-VARIABLE: INSTALL_LMOD
# @DESCRIPTION:
# Usable in phase functions. Set to the "INSTALL_LMOD" variable, as
# given by pkg-config, for the relevant lua target

# @ECLASS-VARIABLE: INSTALL_CMOD
# @DESCRIPTION:
# Usable in phase functions. Set to the "INSTALL_CMOD" variable, as
# given by pkg-config, for the relevant lua target

# @FUNCTION: _lua_validate_useflags
# @INTERNAL
# @DESCRIPTION:
# Enforce the proper setting of LUA_TARGETS
_lua_validate_useflags() {
	debug-print-function ${FUNCNAME} "${@}"

	local i

	for i in "${_LUA_ALL_IMPLS[@]}"; do
		use "lua_targets_${i}" && return 0
	done

	eerror "No Lua implementation selected for the build. Please add one"
	eerror "of the following values to your LUA_TARGETS (in make.conf):"
	eerror
	eerror "${LUA_COMPAT[@]}"
	echo
	die "No supported Lua implementation in LUA_TARGETS."
}

# @ECLASS-VARIABLE: BUILD_DIR
# @DESCRIPTION:
# The current build directory. In global scope, it is supposed to
# contain an initial build directory; if unset, it defaults to ${S}.
#
# In functions run by lua_foreach_impl(), the BUILD_DIR is locally
# set to an implementation-specific build directory. That path is
# created through appending a hyphen and the implementation name
# to the final component of the initial BUILD_DIR.
#
# Example value:
# @CODE
# ${WORKDIR}/foo-1.3-lua5_3
# @CODE

# @FUNCTION: lua_copy_sources
# @DESCRIPTION:
# Create a single copy of the package sources for each enabled Lua
# implementation.
#
# The sources are always copied from initial BUILD_DIR (or S if unset)
# to implementation-specific build directory matching BUILD_DIR used by
# lua_foreach_abi().
lua_copy_sources() {
	debug-print-function ${FUNCNAME} "${@}"

	pushd "${S}"

	local MULTIBUILD_VARIANTS
	_lua_obtain_impls

	multibuild_copy_sources
	popd
}

# @FUNCTION: _lua_obtain_impls
# @INTERNAL
# @DESCRIPTION:
# Set up the enabled implementation list.
_lua_obtain_impls() {
	_lua_validate_useflags

	MULTIBUILD_VARIANTS=()

	local impl
	for impl in "${_LUA_ALL_IMPLS[@]}"; do
		has "${impl}" "${LUA_COMPAT[@]}" && \
		use "lua_targets_${impl}" && MULTIBUILD_VARIANTS+=( "${impl}" )
	done
}

# @FUNCTION: _lua_multibuild_wrapper
# @USAGE: <command> [<args>...]
# @INTERNAL
# @DESCRIPTION:
# Initialize the environment for Lua implementation selected
# for multibuild.
_lua_multibuild_wrapper() {
	debug-print-function ${FUNCNAME} "${@}"
	local l=$(_lua_pkgconfig_from_target "${MULTIBUILD_VARIANT}")

	local -x LUA_INCLUDEDIR=$($(tc-getPKG_CONFIG) --variable includedir "${l}")
	local -x LUA_LIBDIR=$($(tc-getPKG_CONFIG) --variable libdir "${l}")
	local -x LUA_V=$($(tc-getPKG_CONFIG) --variable V "${l}")
	[ -z "${LUA_V}" ] && LUA_V=$($(tc-getPKG_CONFIG) --variable abiver "${l}")
	local -x INSTALL_LMOD=$($(tc-getPKG_CONFIG) --variable INSTALL_LMOD "${l}")
	local -x INSTALL_CMOD=$($(tc-getPKG_CONFIG) --variable INSTALL_CMOD "${l}")
	local -x CFLAGS="${CFLAGS} $($(tc-getPKG_CONFIG) --cflags ${l})"
	local -x LDFLAGS="${LDFLAGS} $($(tc-getPKG_CONFIG) --libs ${l})"
	local -x prefix="${ED}"

	pushd "${BUILD_DIR}"
	"${@}"
	popd
}

# @FUNCTION: lua_foreach_impl
# @USAGE: <command> [<args>...]
# @DESCRIPTION:
# Run the given command for each of the enabled Lua implementations.
# If additional parameters are passed, they will be passed through
# to the command.
#
# The function will return 0 status if all invocations succeed.
# Otherwise, the return code from first failing invocation will
# be returned.
#
# For each command being run, ELUA, LUA and BUILD_DIR are set
# locally, and the former two are exported to the command environment.
lua_foreach_impl() {
	debug-print-function ${FUNCNAME} "${@}"

	local MULTIBUILD_VARIANTS
	_lua_obtain_impls

	multibuild_foreach_variant _lua_multibuild_wrapper "${@}"
}

###

lua-alt_src_prepare() {
	debug-print-function ${FUNCNAME} "${@}"
	eapply_user
	lua_copy_sources
	lua_foreach_impl default
}

lua-alt_src_configure() {
	debug-print-function ${FUNCNAME} "${@}"
	lua_foreach_impl default
}

lua-alt_src_compile() {
	debug-print-function ${FUNCNAME} "${@}"
	lua_foreach_impl default
}

lua-alt_src_test() {
	debug-print-function ${FUNCNAME} "${@}"
	lua_foreach_impl default
}

lua-alt_src_install() {
	debug-print-function ${FUNCNAME} "${@}"
	lua_foreach_impl default
}
