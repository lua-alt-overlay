# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: lua-utils-alt.eclass
# @MAINTAINER:
# ???????
# @AUTHOR:
# S. Gilles <sgilles@math.umd.edu>
# Based on work of: Michał Górny <mgorny@gentoo.org>
# Based on work of: Krzysztof Pawlik <nelchael@gentoo.org>
# @SUPPORTED_EAPIS: 6 7
# @BLURB: Utility functions for packages with Lua parts.
# @DESCRIPTION:
# A utility class providing functions to query Lua implementations
#
# This eclass does not export any phase functions.
#
# This eclass sets correct IUSE. Modification of REQUIRED_USE has
# to be done by the author of the ebuild.

case "${EAPI:-0}" in
	0|1|2|3|4|5)
		die "Unsupported EAPI=${EAPI:-0} (too old) for ${ECLASS}"
		;;
	6|7)
		;;
	*)
		die "Unsupported EAPI=${EAPI} (unknown) for ${ECLASS}"
		;;
esac


inherit multibuild
inherit toolchain-funcs

# @ECLASS-VARIABLE: LUA_COMPAT
# @REQUIRED
# @DESCRIPTION:
# This variable contains a list of Lua implementations the package
# supports. It must be set before the `inherit' call. It has to be
# an array.
#
# Example:
# @CODE
# LUA_COMPAT=( lua5_4 lua5_3 luajit2 )
# @CODE

# @ECLASS-VARIABLE: _LUA_ALL_IMPLS
# @INTERNAL
# @DESCRIPTION:
# All supported Lua implementations
_LUA_ALL_IMPLS=( luajit2 lua5_1 lua5_2 lua5_3 lua5_4 )
readonly _LUA_ALL_IMPLS

# @FUNCTION _lua_package_from_target
# @INTERNAL
# @DESCRIPTION:
# Given "lua5_3", return "dev-lang/lua:5.3" or such
_lua_package_from_target() {
	[ "$1" == "lua5_4" ] && echo "dev-lang/lua:5.4"
	[ "$1" == "lua5_3" ] && echo "dev-lang/lua:5.3"
	[ "$1" == "lua5_2" ] && echo "dev-lang/lua:5.2"
	[ "$1" == "lua5_1" ] && echo "dev-lang/lua:5.1"
	[ "$1" == "luajit2" ] && echo "dev-lang/luajit:2"
}

# @FUNCTION: _lua_pkgconfig_from_target
# @INTERNAL
# DESCRIPTION:
# Given a name from LUA_COMPAT, return the appropriate name for pkg-config
_lua_pkgconfig_from_target() {
	[ "$1" == "lua5_4" ] && echo "lua5.4"
	[ "$1" == "lua5_3" ] && echo "lua5.3"
	[ "$1" == "lua5_2" ] && echo "lua5.2"
	[ "$1" == "lua5_1" ] && echo "lua5.1"
	[ "$1" == "luajit2" ] && echo "luajit"
}

# @FUNCTION: _lua_set_impls
# @INTERNAL
# @DESCRIPTION:
#  Check LUA_COMPAT for well-formedness and validity, then set
#  _LUA_IMPLS to be the intersection of LUA_COMPAT and _LUA_ALL_IMPLS.
#  Also set IUSE.
_lua_set_globals() {
	debug-print-function ${FUNCNAME} "${@}"
	local deps i

	if ! declare -p LUA_COMPAT &>/dev/null; then
		die 'LUA_COMPAT not declared.'
	fi
	if [[ $(declare -p LUA_COMPAT) != "declare -a"* ]]; then
		die 'LUA_COMPAT must be an array.'
	fi

	_LUA_IMPLS=()

	for i in "${_LUA_ALL_IMPLS[@]}"; do
		if has "${i}" "${LUA_COMPAT[@]}"; then
			_LUA_IMPLS+=( "${i}" )
		fi
	done

	if [[ ! ${_LUA_IMPLS[@]} ]]; then
		die "No supported implementation in LUA_COMPAT."
	fi

	for i in "${_LUA_IMPLS[@]}"; do
		deps+="lua_targets_${i}? ( $(_lua_package_from_target ${i}) ) "
	done

	local flags=( "${_LUA_ALL_IMPLS[@]/#/lua_targets_}" )
	local requse="|| ( ${flags[*]} )"

	local f=( )
	for (( i = ${#_LUA_ALL_IMPLS[@]} - 1; i >= 0; i-- )); do
		f+=( "lua_targets_${_LUA_ALL_IMPLS[i]}" )
	done
	IUSE=${f[*]}

	LUA_DEPS=${deps}
	LUA_REQUIRED_USE=${requse}
	readonly LUA_DEPS LUA_REQUIRED_USE
}
_lua_set_globals
unset -f _lua_set_globals
